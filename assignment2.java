import java.util.*;
import java.text.*;

public class assignment2 {
	public static void main(String args[]) {
		DecimalFormat df = new DecimalFormat(".##");
		DecimalFormat mxm = new DecimalFormat("");
		Scanner Sc = new Scanner(System.in);
		System.out.println("Please decide how many numbers you want to input");
		int k = Sc.nextInt();
		double a[] = new double[k];
		System.out.println("Please input integer numbers");
		System.out.println("Interger range is [-2147483647,2147483647]");
		for (int i = 0; i < k; i++) {
			System.out.println("Please input integer Number " + (i + 1));
			a[i] = Sc.nextInt();
		}
		Arrays.sort(a);
		double Max = 0, sum = 0, Min = 2147483647, Med = 0, maxValue = 0, maxCount = 0;
		for (int i = 0; i < k; i++) {
			sum = sum + a[i];
		}// sum
		double average = sum / k; // Average
		// Maximum
		Max = a[k - 1];
		// Maximum
		Min = a[0];
		// Minimum
		for (int i = 0; i < a.length; i++) {
			if (a[i] < Min)
				Min = a[i];
			// Minimum
			// Mode
			for (int m = 1; m < a.length; m++) {
				int count = 0;
				for (int x = 0; x < a.length; x++) {
					if (a[x] == a[m])
						count++;
					if (count > maxCount) {
						maxCount = count;
						maxValue = a[m];
					}
				}
			}
			// Mode
			// median
			if ((k % 2) == 0) {
				double b[] = new double[k];
				for (int t = 0; t < k; t++) {
					b[t] = a[t];
				}
				Med = (b[k / 2] + b[(k / 2) - 1]) / 2;
			} else if ((k % 2) == 1) {
				double b[] = new double[k];
				for (int t = 0; t < k; t++) {
					b[t] = a[t];
				}
				Med = b[(k - 1) / 2];
				// median
			}
		}
		// Printout section//
		System.out.println("Sum: " + mxm.format(sum));
		System.out.println("Average of all your number is "
                           + df.format(average) + ".");
		System.out.println("The maximum of the array is " + mxm.format(Max)
                           + ".");
		System.out.println("The minimum of the array is " + mxm.format(Min)
                           + ".");
		System.out.println("The median number of the array is "
                           + mxm.format(Med) + ".");
		if (maxCount < 2) {
			System.out.println("The mode of the array does not exist.");
		} else {
			System.out.println("The mode of the array is "
                               + mxm.format(maxValue) + ", it appeared " + mxm.format(maxCount)
                               + " times.");
		}
	}
}
